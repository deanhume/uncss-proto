## Remove Unused CSS

This is a simple repo that contains the setup for Grunt UnCSS. Point it at any URL and it will remove the unused CSS for a page.

Simply edit the gruntfile.js to achieve this.
